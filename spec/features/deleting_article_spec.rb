require 'rails_helper'

RSpec.feature 'Deleting an article' do
    before do
        @article = Article.create(title: "The First Article",  body: "Lorem ipsum of first article")
    end
    
    scenario "User deletes an article" do
       visit '/'
       click_link @article.title
       click_link "Delete Article"
       
       expect(page).to have_content "Article has been deleted"
       expect(page).not_to have_content @article.title
       expect(page.current_path).to eq root_path
    end
    
end