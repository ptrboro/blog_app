require 'rails_helper'

RSpec.feature "Creating Articles" do
    scenario "User creates new article" do
        visit "/"
        
        click_link "New Article"
        
        fill_in "Title", with: "Creating a blog"
        fill_in "Body", with: "Lorem Ipsum"
        
        click_button "Create Article"
        
        expect(page).to have_content("Article has been created")
        expect(page.current_path).to eq(articles_path)
    end
    
    scenario "User fails to create article" do 
       visit new_article_path
       
       fill_in "Title", with: ""
       fill_in "Body", with: ""
       
       click_button "Create Article"
       
       expect(page).to have_content("Article has not been created")
       expect(page).to have_content("Title can't be blank")
       expect(page).to have_content("Body can't be blank")
       
       visit "/"
       
       expect(page).to have_no_content("Article has not been created")
    end
end