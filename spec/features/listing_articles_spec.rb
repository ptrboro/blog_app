require 'rails_helper'

RSpec.feature "Listing Articles" do
    before do
        @articles = []
        @articles << Article.create(title: "The First Article",  body: "Lorem ipsum of first article")
        @articles << Article.create(title: "The Second Article", body: "Lorem ipsum of second article")
    end
    
    scenario "User lists all articles" do
        visit '/'
        
        @articles.each do |article|
            expect(page).to have_content(article.title)
            expect(page).to have_content(article.body[0...497])
            expect(page).to have_link(article.title)
        end

    end
    
    scenario "Article has body longer than 500 chars" do
        @articles << @long_article = Article.create(title: "I have long body",   body: "Very long body Lorem Ipsum " * 20 )
        visit '/'
        
        expect(page).to     have_content(@long_article.body[0...497] + '...')
        expect(page).not_to have_content(@long_article.body)
    end
    
    scenario "A user has no articles" do 
       Article.delete_all
       
       visit '/'
       
       @articles.each do |article|
            expect(page).not_to have_content(article.title)
            expect(page).not_to have_content(article.body[0...497])
            expect(page).not_to have_link(article.title)
        end
        
        within ("h1#no-articles") do
           expect(page).to have_content "No Articles to Display" 
        end
        
    end
end