require 'rails_helper'

RSpec.feature "Editing an article" do
    before do
        @article = Article.create(title: "The First Article",  body: "Lorem ipsum of first article")
    end
    
    scenario "User updates an article" do
       visit '/'
       click_link @article.title
       click_link "Edit Article"
       
       fill_in "Title", with: "Edited Title"
       fill_in "Body", with: "Updated Lorem ipsum of first article"
       click_button "Update Article"
       
       expect(page).to have_content "Article has been updated"
       expect(page.current_path).to eq article_path(@article)
       expect(page).to have_content "Edited Title"
       expect(page).to have_content "Updated Lorem ipsum of first article"
    end
    
    scenario "User fails to update an article" do
       visit '/'
       click_link @article.title
       click_link "Edit Article"
       
       fill_in "Title", with: ""
       fill_in "Body", with: "Updated Lorem ipsum of first article"
       click_button "Update Article"
       
       expect(page).to have_content "Article has not been updated"
       expect(page.current_path).to eq article_path(@article)
       expect(page).to have_content @article.body
    end
end