require 'rails_helper'

RSpec.describe "Articles", type: :request do
    
    before do
       @article = Article.create(title: "The First Article",  body: "Lorem ipsum of first article")
    end
    
    describe "GET /articles/:id" do
        context 'with existing article' do
           before { get "/articles/#{@article.id}" }
           
           it "handles existing article" do
              expect(response.status).to eq 200
           end
        end
        
        context 'with non existing article' do
           before { get "/articles/xxx" }
           
           it "handles non existing article" do
               expect(response.status).to eq 302
               flash_message = "Article you are looking for could not be found"
               expect(flash[:warning]).to eq flash_message
           end
        end
    end
end