# README

* BLOG APPLICATION - LEARNING BEHAVIOR DRIVEN DEVELOPMENT WITH RSPEC & CAPYBARA
* 

* bundle install     <- install gems
* rails db:migrate   <- migrate database
* rails db:seed      <- populate database
* rspec              <- run tests
* rails s            <- start server
