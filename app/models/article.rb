class Article < ApplicationRecord
    
    default_scope { order(created_at: :desc) }
    
    validates :title, presence: true, length: { in: 4..200 }
    validates :body, presence: true
end
